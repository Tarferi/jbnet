package org.spigot.jBNLBOT.BNETpackets;

import java.nio.ByteBuffer;

import org.spigot.jBNLBOT.GamePlayer;
import org.spigot.jBNLBOT.Exceptions.StringNotFoundException;

public class Buffer {
	private ByteBuffer buff = ByteBuffer.allocate(300);
	public byte pid;
	public byte cid;
	public byte status = 0;
	public int recv, sent;
	public GamePlayer target;
	public byte playerID;
	public boolean increment=false;

	public boolean hostdiffers() {
		return target != null;
	}

	public Buffer(byte pid) {
		this.pid = pid;
	}

	public Buffer(byte pid, byte cid) {
		this.pid = pid;
		this.cid = cid;
	}

	public void resetPosition() {
		buff.position(0);
	}

	public static final String getDump(byte[] data) {
		StringBuilder sb = new StringBuilder("DUMP:");
		int i=0;
		for (byte b : data) {
			i++;
			if(i==256) {
				sb.append(" ..");
				break;
			}
			sb.append(" " + rep(Integer.toHexString(b & 0xff)));
		}
		return sb.toString();
	}

	private static String rep(String hex) {
		if (hex.length() == 1) {
			return "0" + hex;
		} else {
			return hex;
		}
	}

	public Buffer() {

	}

	/*
	 * public void putUnsignedDword(long w) { buff.put((byte) (w & 0xff)); w >>= 8; buff.put((byte) (w & 0xff)); w >>= 8; buff.put((byte) (w & 0xff)); w >>= 8; buff.put((byte) (w & 0xff)); }
	 */
	/*
	 * public long getUnsignedDword() { int i1, i2, i3, i4; i1 = buff.get() & 0xff; i2 = buff.get() & 0xff; i3 = buff.get() & 0xff; i4 = buff.get() & 0xff; System.out.println("I: " + i1 + "." + i2 + "." + i3 + "." + i4); return i1 | (i2 << 8) | (i3 << 16) | (i4 << 24); //return ((buff.get() & 0xff) + ((buff.get() & 0xff) << 8) + ((buff.get() & 0xff) << 16) + ((buff.get() & 0xff) << 24)); }
	 */

	public Buffer(byte[] b) {
		buff = ByteBuffer.wrap(b);
	}

	public int getLength() {
		return buff.position();
	}

	public int getTotalLength() {
		return buff.capacity();
	}

	public byte getPid() {
		return pid;
	}

	public int getDword() {
		return ((getByte() << 0) & 0x000000FF) | ((getByte() << 8) & 0x0000FF00) | ((getByte() << 16) & 0x00FF0000) | ((getByte() << 24) & 0xFF000000);
	}

	public int getWord() {
		int ret = ((getByte() << 0) & 0x0000FF) | ((getByte() << 8) & 0x00FF00);
		return (ret & 0x0000FFFF);
	}

	public void putLong(long l) {
		putByte((byte) ((l & 0x00000000000000FFL) >> 0L));
		putByte((byte) ((l & 0x000000000000FF00L) >> 8L));
		putByte((byte) ((l & 0x0000000000FF0000L) >> 16L));
		putByte((byte) ((l & 0x00000000FF000000L) >> 24L));
		putByte((byte) ((l & 0x000000FF00000000L) >> 32L));
		putByte((byte) ((l & 0x0000FF0000000000L) >> 40L));
		putByte((byte) ((l & 0x00FF000000000000L) >> 48L));
		putByte((byte) ((l & 0xFF00000000000000L) >> 56L));
	}

	public long getLong() throws IndexOutOfBoundsException {
		return (((long) getByte() << 0L) & 0x00000000000000FFL) | (((long) getByte() << 8L) & 0x000000000000FF00L) | (((long) getByte() << 16L) & 0x0000000000FF0000L) | (((long) getByte() << 24L) & 0x00000000FF000000L) | (((long) getByte() << 32L) & 0x000000FF00000000L) | (((long) getByte() << 40L) & 0x0000FF0000000000L) | (((long) getByte() << 48L) & 0x00FF000000000000L) | (((long) getByte() << 56L) & 0xFF00000000000000L);
	}

	public void putWord(short w) {
		putByte((byte) ((w & 0x00FF) >> 0));
		putByte((byte) ((w & 0xFF00) >> 8));
	}

	public byte getByte() {
		return buff.get();
	}

	public byte[] getBytes(int len) {
		byte[] b = new byte[len];
		buff.get(b);
		return b;
	}

	public long getFileTime() {
		return getLong();
	}

	public byte[] getData() {
		int pos = buff.position();
		byte[] b = new byte[pos];
		buff.position(0);
		buff.get(b);
		buff.position(pos);
		return b;
	}

	public byte[] getData(boolean all) {
		int pos = buff.position();
		byte[] b = new byte[buff.capacity()];
		buff.position(0);
		buff.get(b);
		buff.position(pos);
		return b;
	}

	public String getString() throws StringNotFoundException {
		StringBuilder s = new StringBuilder();
		int max = buff.capacity();
		for (int j = 0; j < max; j++) {
			char c = (char) getByte();
			if (c == 0) {
				return s.toString();
			}
			s.append(c);
		}
		throw new StringNotFoundException();
	}

	public void putByteString(byte[] b) {
		buff.put(b);
		buff.put((byte) 0);
	}

	public byte[] getByteString() throws StringNotFoundException {
		int max = buff.capacity();
		ByteBuffer buff0 = ByteBuffer.allocate(max);
		for (int j = 0; j < max; j++) {
			byte c = getByte();
			if (c == 0) {
				byte[] b = new byte[buff0.position()];
				buff0.position(0);
				buff0.get(b);
				return b;
			}
			buff0.put(c);
		}
		throw new StringNotFoundException();
	}

	/*
	 * public String getString() throws StringNotFoundException { StringBuilder sb = new StringBuilder(); int max = buff.capacity(); for (int i = buff.position(); i < max; i++) { byte b = buff.get(); if (b == 0) { return sb.toString(); } else { sb.append(Character.toString((char) b)); } } throw new StringNotFoundException(); }
	 */

	/*
	 * public void putDword(int w) { buff.put((byte) (w & 0xff)); w >>= 8; buff.put((byte) (w & 0xff)); w >>= 8; buff.put((byte) (w & 0xff)); w >>= 8; buff.put((byte) (w & 0xff)); }
	 */
	public void putDword(int d) {
		putByte((byte) ((d & 0x000000FF) >> 0));
		putByte((byte) ((d & 0x0000FF00) >> 8));
		putByte((byte) ((d & 0x00FF0000) >> 16));
		putByte((byte) ((d & 0xFF000000) >> 24));
	}

	public void putByte(int b) {
		buff.put((byte) (b & 0xff));
	}

	public void putBytes(byte[] b) {
		buff.put(b);
	}

	public void putString(String string) {
		buff.put(string.getBytes());
	}

	public void putNTString(String string) {
		byte[] b = string.getBytes();
		for (int j = 0; j < b.length; j++) {
			putByte((b[j] & 0xff));
		}
		putByte(0);
	}

	public void aputNTString(String string) {
		buff.put(string.getBytes());
		buff.put((byte) 0);
	}
}
