package org.spigot.jBNLBOT.BNETpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNLSpackets.BNLS_VERSIONCHECKEX2.BNLS_AUTH;
import org.spigot.jBNLBOT.Exceptions.LoginFailureException;
import org.spigot.jBNLBOT.Exceptions.StringNotFoundException;


public class SID_AUTH_CHECK extends packet {

	public static void Write(packet packet, BNLS_AUTH auth, int serverToken, String cdkey) throws IOException {
		Buffer buff = new Buffer((byte) 0x51);
		buff.putDword(auth.CdKeyHash.ClientToken);
		buff.putDword(auth.ExeVersion);
		buff.putDword(auth.ExeHash);
		buff.putDword(1);
		buff.putDword(0);
		buff.putDword(auth.CdKeyHash.KeyLength);
		buff.putDword(auth.CdKeyHash.KeyProductValue);
		buff.putDword(auth.CdKeyHash.KeyPublicValue);
		buff.putDword(auth.CdKeyHash.Unknown);
		buff.putDword(auth.CdKeyHash.Hash[0]);
		buff.putDword(auth.CdKeyHash.Hash[1]);
		buff.putDword(auth.CdKeyHash.Hash[2]);
		buff.putDword(auth.CdKeyHash.Hash[3]);
		buff.putDword(auth.CdKeyHash.Hash[4]);
		buff.putByteString(auth.ExeInfo);
		buff.putNTString("iThief");
		packet.send(buff);
	}

	public static void Read(Buffer buff) throws StringNotFoundException, LoginFailureException {
		int result = buff.getDword();
		String details = buff.getString();
		if (result == 0x100) {
			throw new LoginFailureException("Old game version");
		} else if (result == 0x101) {
			throw new LoginFailureException("Invalid version");
		} else if (result == 0x102) {
			throw new LoginFailureException("Game must be downgraded");
		} else if (result == 0x200) {
			throw new LoginFailureException("Invalid CD key");
		} else if (result == 0x201) {
			throw new LoginFailureException("CD key in use by " + details);
		} else if (result == 0x202) {
			throw new LoginFailureException("CD key is banned");
		} else if (result == 0x203) {
			throw new LoginFailureException("Wrong product");
		} else if (result == 0) {
			// Passed
		} else {
			throw new LoginFailureException("Unknown cause");
		}
	}
}
