package org.spigot.jBNLBOT.BNETpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;
import org.spigot.jBNLBOT.Exceptions.StringNotFoundException;

public class SID_AUTH_INFO extends packet {

	public class AUTH_INFO {
		public final int LogonType, UDPValue;
		public final long MPQ;
		public final int ServerToken;
		public final String CdKey;
		public final String VerFilename;
		public final byte[] ValueString;

		private AUTH_INFO(int lt, int st, int uv, long mpq, String vf, byte[] vs, String cdkey) {
			this.LogonType = lt;
			this.ServerToken = st;
			this.UDPValue = uv;
			this.MPQ = mpq;
			this.VerFilename = vf;
			this.ValueString = vs;
			this.CdKey = cdkey;
		}
	}

	public static AUTH_INFO Read(Buffer buff, String cdkey) throws StringNotFoundException {
		int logontype = buff.getDword();
		int servertoken = buff.getDword();
		int udpvalue = buff.getDword();
		long fp = buff.getFileTime();
		String fn = buff.getString();
		byte[] vs = buff.getByteString();
		return new SID_AUTH_INFO().new AUTH_INFO(logontype, servertoken, udpvalue, fp, fn, vs, cdkey);
	}

	public static void Write(packet pack, byte[] localip) throws IOException {
		Buffer buff = new Buffer((byte) 0x50);
		buff.putDword(0); // Protocol ID
		buff.putString("68XI"); // Platform ID
		buff.putString("PXES"); // Product ID
		buff.putDword(0xD3); // Version byte
		buff.putString("csCZ"); // Product language
		buff.putByte(localip[0]);
		buff.putByte(localip[1]);
		buff.putByte(localip[2]);
		buff.putByte(localip[3]);
		// buff.putDword(0); // LAN IP
		buff.putDword(0); // Timezone and bias
		buff.putDword(1029); // Locale ID
		buff.putDword(1029); // Language ID
		buff.putNTString("CZE");
		buff.putNTString("Czech Republic");
		pack.send(buff);
	}
}
