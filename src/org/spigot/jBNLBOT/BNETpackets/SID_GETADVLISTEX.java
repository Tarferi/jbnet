package org.spigot.jBNLBOT.BNETpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.StarcraftGame;
import org.spigot.jBNLBOT.Exceptions.StringNotFoundException;

public class SID_GETADVLISTEX extends packet {

	@SuppressWarnings("unused")
	public static StarcraftGame[] read(Buffer buff) throws StringNotFoundException {
		int pocet = buff.getDword();
		if (pocet == 0) {
			int status = buff.getDword();
		} else {
			StarcraftGame[] games = new StarcraftGame[pocet];
			for (int i = 0; i < pocet; i++) {
				int gametype = buff.getWord();
				int subgametype = buff.getWord();
				int languageid = buff.getDword();
				int addressfam = buff.getWord();
				int port = buff.getWord();
				int ip = buff.getDword();
				buff.getDword();
				buff.getDword();
				int status = buff.getDword();
				int time = buff.getDword();
				String name = buff.getString();
				String pass = buff.getString();
				String stat = buff.getString();
				games[i] = new StarcraftGame(addressfam, port, ip, time, name, pass, stat);
			}
			return games;
		}
		return null;
	}

	public static void write(packet pack) throws IOException {
		Buffer buff = new Buffer((byte) 0x09);
		buff.putWord((short) 0); // Game filter
		buff.putWord((short) 1); // Game type
		buff.putDword(0); // View filter
		buff.putDword(0); // reserved
		buff.putDword(0xff); // Number of games
		buff.putNTString("");
		buff.putNTString("");
		buff.putNTString("");
		pack.send(buff);
	}
}
