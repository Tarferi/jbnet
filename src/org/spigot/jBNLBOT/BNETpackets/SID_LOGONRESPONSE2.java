package org.spigot.jBNLBOT.BNETpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.Exceptions.AccountClosedException;
import org.spigot.jBNLBOT.Exceptions.InvalidPasswordException;
import org.spigot.jBNLBOT.Exceptions.LoginFailException;
import org.spigot.jBNLBOT.Exceptions.StringNotFoundException;
import org.spigot.jBNLBOT.Exceptions.UserDoesNotExistException;

import Hashing.DoubleHash;

public class SID_LOGONRESPONSE2 extends packet {
	public static void read(Buffer buff) throws UserDoesNotExistException, InvalidPasswordException, AccountClosedException, StringNotFoundException, LoginFailException {
		int status=buff.getDword();
		if(status==1) {
			throw new UserDoesNotExistException();
		} else if(status==2) {
			throw new InvalidPasswordException();
		} else if(status==6) {
			throw new AccountClosedException(buff.getString());
		} else if(status==0) {
			//Passed
		} else {
			throw new LoginFailException(buff.getString());
		}
		
		
	}
	
	public static void write(packet pack, int ServerToken, int ClientToken, String password, String username) throws IOException {
		Buffer buff = new Buffer((byte) 0x3a);
		int[] r = DoubleHash.doubleHash(password, ClientToken, ServerToken);
		buff.putDword(ClientToken);
		buff.putDword(ServerToken);
		buff.putDword(r[0]);
		buff.putDword(r[1]);
		buff.putDword(r[2]);
		buff.putDword(r[3]);
		buff.putDword(r[4]);
		buff.putNTString(username);
		System.out.println("Logging in as "+username);
		pack.send(buff);
	}
}
