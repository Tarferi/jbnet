package org.spigot.jBNLBOT.BNETpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class SID_PING extends packet {

	public static int read(packet pack) throws IOException {
		return pack.readDword();
	}
	
	public static void write(packet pack, int ping) throws IOException {
		Buffer buff=new Buffer((byte) 0x25);
		buff.putDword(ping);
		pack.send(buff);
	}
}
