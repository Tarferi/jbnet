package org.spigot.jBNLBOT.BNETpackets;

import java.io.IOException;

public class SID_STARTADVEX3 extends packet {

	public static boolean read(Buffer buff) {
		int i=buff.getDword();
		return i==0;
	}
	
	public static void write(packet pack, String gameName, String password, String MapName, String HostName, int map_x, int map_y) throws IOException {
		Buffer buff = new Buffer((byte) 0x1c);
		buff.putDword(0);
		buff.putDword(0);
		buff.putWord((short) 0x0a);
		buff.putWord((short) (1));
		buff.putDword(0xff);
		buff.putDword(0);
		buff.putNTString(gameName);
		buff.putNTString(password);
		String s=","+(map_x*32)+""+(map_y*32)+",,6,1,a,,,Nope LOL,7,,"+HostName+new String(new byte[]{0x0d})+MapName+new String(new byte[]{0x0d});
		buff.putNTString(s);
		pack.send(buff);
	}
}
