package org.spigot.jBNLBOT.BNETpackets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import org.spigot.jBNLBOT.Exceptions.DisconnectedException;

public class packet {
	private InputStream in;
	private OutputStream out;
	public int pid;
	public Buffer data;

	private packet(int pid, byte[] data) {
		this.pid = pid;
		this.data = new Buffer(data);
	}

	public packet(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
	}

	protected packet() {
	}

	public synchronized void send(Buffer buff) throws IOException {
		byte[] b = addHeader(buff);
		out.write(ByteBuffer.allocate(b.length + buff.getData().length).put(b).put(buff.getData()).array());
		out.flush();
	}

	public packet getNextPacket() throws DisconnectedException, IOException {
		int inp = in.read();
		if (inp == -1) {
			throw new DisconnectedException();
		}
		if (inp != 0xff) {
			throw new DisconnectedException();
		}
		byte pid = (byte) in.read();
		int i1 = readByte();
		int i2 = readByte();
		if (i1 < 0) {
			i1 += 256;
		}
		if (i2 < 0) {
			i2 += 256;
		}
		int len = i1 | (i2 << 8);
		len -= 4;
		if (len <= 0) {
			if (len < 0) {
				System.out.println("Invalid length: " + pid + " (" + len + ")");
			}
			return null;
		}
		byte[] data = new byte[len];
		in.read(data);
		packet pack = new packet(pid, data);
		return pack;
	}

	public byte readByte() throws IOException {
		return (byte) in.read();
	}

	public int readShort() throws IOException {
		return (int) ((readByte() & 0xffffffff) | ((readByte() & 0xffffffff) << 8));
	}

	private byte[] addHeader(Buffer buff) {
		return new byte[] { (byte) 0xFF, buff.getPid(), ((byte) ((buff.getLength() + 4) & 0x00FF)), (byte) ((buff.getLength() + 4) & 0xFF00) };
	}

	public int readDword() throws IOException {
		return (in.read()) | (in.read() << 8) | (in.read() << 16) | (in.read() << 24);
	}
}
