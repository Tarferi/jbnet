package org.spigot.jBNLBOT;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.spigot.jBNLBOT.BNETpackets.SID_AUTH_INFO.AUTH_INFO;
import org.spigot.jBNLBOT.BNLSpackets.BNLS_CDKEY_EX;
import org.spigot.jBNLBOT.BNLSpackets.BNLS_VERSIONCHECKEX2;
import org.spigot.jBNLBOT.BNLSpackets.BNLS_VERSIONCHECKEX2.BNLS_AUTH;
import org.spigot.jBNLBOT.BNLSpackets.packet;
import org.spigot.jBNLBOT.Exceptions.BNLSFailException;
import org.spigot.jBNLBOT.Exceptions.CdKeyBNLSFailException;
import org.spigot.jBNLBOT.Exceptions.DisconnectedException;
import org.spigot.jBNLBOT.Exceptions.StringNotFoundException;

public class BNLSHandler {

	public static BNLS_AUTH getData(AUTH_INFO inf, String bnls) throws IOException, DisconnectedException, BNLSFailException, StringNotFoundException, CdKeyBNLSFailException {
		Socket sock = new Socket(bnls, 9367);
		InputStream in = sock.getInputStream();
		OutputStream out = sock.getOutputStream();
		packet packet = new packet(in, out);
		BNLS_VERSIONCHECKEX2.Write(packet, inf);
		byte ver = 0;
		boolean cont = true;
		BNLS_AUTH auth = null;
		while (cont) {
			packet pack = packet.getNextPacket();
			switch (pack.pid) {
				default:
					System.out.println("Received unknown BNLS response: " + pack.pid);
				break;
				case 0x1A:
					try {
						auth = BNLS_VERSIONCHECKEX2.Read(pack.data, ver);
						BNLS_CDKEY_EX.Write(packet, inf.CdKey, inf.ServerToken);
					} catch (Exception e) {
						e.printStackTrace();
					}
				break;
				case 0x0c:
					auth.CdKeyHash = BNLS_CDKEY_EX.Read(pack.data);
					cont = false;
				break;
			}
		}
		try {
			in.close();
			out.close();
			sock.close();
		} catch (Exception e) {

		}
		if (auth == null) {
			throw new BNLSFailException("Not found");
		}
		return auth;
	}
}
