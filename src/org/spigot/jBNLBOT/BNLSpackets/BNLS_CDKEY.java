package org.spigot.jBNLBOT.BNLSpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;
import org.spigot.jBNLBOT.Exceptions.CdKeyBNLSFailException;

public class BNLS_CDKEY extends packet {

	public class CDKEY_HASH {
		public final int ClientToken, CdKeyLength, CdKeyProductValue, CdKeyPublicValue, Unknown, Hash1, Hash2, Hash3, Hash4, Hash5;

		private CDKEY_HASH(int ct, int keylen, int keyprod, int keypub, int unkn, int h1, int h2, int h3, int h4, int h5) {
			ClientToken = ct;
			CdKeyLength = keylen;
			CdKeyProductValue = keyprod;
			CdKeyPublicValue = keypub;
			Unknown = unkn;
			Hash1 = h1;
			Hash2 = h2;
			Hash3 = h3;
			Hash4 = h4;
			Hash5 = h5;
		}

	}

	public static CDKEY_HASH Read(Buffer buff) throws CdKeyBNLSFailException {
		int res = buff.getDword();
		if (res != 1) {
			throw new CdKeyBNLSFailException();
		}
		int ct = buff.getDword();
		int kl = buff.getDword();
		int kp = buff.getDword();
		int kpu = buff.getDword();
		int un = buff.getDword();
		int hash1 = buff.getDword();
		int hash2 = buff.getDword();
		int hash3 = buff.getDword();
		int hash4 = buff.getDword();
		int hash5 = buff.getDword();
		return new BNLS_CDKEY().new CDKEY_HASH(ct, kl, kp, kpu, un, hash1, hash2, hash3, hash4, hash5);
	}

	public static void Write(packet pack, String key, int token) throws IOException {
		Buffer buff = new Buffer((byte) 0x01);
		buff.putDword(token);
		buff.putNTString(key);
		pack.send(buff);
	}

}
