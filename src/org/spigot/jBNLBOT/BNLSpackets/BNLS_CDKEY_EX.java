package org.spigot.jBNLBOT.BNLSpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;
import org.spigot.jBNLBOT.Exceptions.CdKeyBNLSFailException;

public class BNLS_CDKEY_EX {

	public class CDKEY_HASH_EX {
		public final int ClientToken, KeyLength, KeyPublicValue, KeyProductValue, Unknown;
		public final int[] Hash;

		private CDKEY_HASH_EX(int[] hashes) {
			ClientToken = hashes[0];
			KeyLength = hashes[1];
			KeyProductValue = hashes[2];
			KeyPublicValue = hashes[3];
			Unknown = hashes[4];
			Hash = new int[] { hashes[5], hashes[6], hashes[7], hashes[8], hashes[9] };
		}

	}

	public static CDKEY_HASH_EX Read(Buffer buff) throws CdKeyBNLSFailException {
		buff.getDword(); // Cookie
		if (buff.getByte() != buff.getByte()) {
			throw new CdKeyBNLSFailException();
		}

		buff.getDword();
		int session = buff.getDword();
		int h1 = buff.getDword();
		int h2 = buff.getDword();
		int h3 = buff.getDword();
		int h4 = buff.getDword();
		int h5 = buff.getDword();
		int h6 = buff.getDword();
		int h7 = buff.getDword();
		int h8 = buff.getDword();
		int h9 = buff.getDword();
		return new BNLS_CDKEY_EX().new CDKEY_HASH_EX(new int[] { session, h1, h2, h3, h4, h5, h6, h7, h8, h9 });
	}

	public static void Write(packet pack, String key, int token) throws IOException {
		Buffer buff = new Buffer((byte) 0x0c);
		buff.putDword(0); // Cookie;
		buff.putByte((byte) 1); // # of keys
		buff.putDword(0); // Flags
		buff.putDword(token);
		buff.putNTString(key);
		pack.send(buff);
	}

}
