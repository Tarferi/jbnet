package org.spigot.jBNLBOT.BNLSpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class BNLS_REQUESTVERSIONBYTE extends packet {

	public static byte Read(Buffer buff) {
		return buff.getByte();
	}
	
	public static void Write(packet pack) throws IOException {
		Buffer buff=new Buffer((byte) 0x10);
		buff.putDword(2);
		pack.send(buff);
	}
}
