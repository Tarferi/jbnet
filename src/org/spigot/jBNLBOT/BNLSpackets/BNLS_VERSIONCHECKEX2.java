package org.spigot.jBNLBOT.BNLSpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;
import org.spigot.jBNLBOT.BNETpackets.SID_AUTH_INFO.AUTH_INFO;
import org.spigot.jBNLBOT.BNLSpackets.BNLS_CDKEY_EX.CDKEY_HASH_EX;
import org.spigot.jBNLBOT.Exceptions.BNLSFailException;
import org.spigot.jBNLBOT.Exceptions.StringNotFoundException;

public class BNLS_VERSIONCHECKEX2 extends packet {

	public class BNLS_AUTH {
		public int ExeVersion, ExeHash;
		public final byte VerByte;
		public final byte[] ExeInfo;
		public CDKEY_HASH_EX CdKeyHash;

		private BNLS_AUTH(int exever, int exehash, byte[] exeinf, byte ver) {
			ExeVersion = exever;
			ExeHash = exehash;
			ExeInfo = exeinf;
			VerByte = ver;
		}
	}

	public static BNLS_AUTH Read(Buffer buff, byte ver) throws BNLSFailException, StringNotFoundException {
		int success = buff.getDword();
		if(success!=1) {
			throw new BNLSFailException("Success: "+success);
		}
		int exever = buff.getDword();
		int exehash = buff.getDword();
		byte[] exeinfo = buff.getByteString();
		buff.getDword();
		buff.getDword();
		return new BNLS_VERSIONCHECKEX2().new BNLS_AUTH(exever, exehash, exeinfo, ver);
	}

	public static void Write(packet pack, AUTH_INFO inf) throws IOException {
		Buffer buff = new Buffer((byte) 0x1A);
		buff.putDword(2);
		buff.putDword(0);
		buff.putDword(1);
		buff.putLong(inf.MPQ);
		buff.putNTString(inf.VerFilename);
		buff.putByteString(inf.ValueString);
		pack.send(buff);
	}
}
