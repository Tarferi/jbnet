package org.spigot.jBNLBOT.BNLSpackets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import org.spigot.jBNLBOT.BNETpackets.Buffer;
import org.spigot.jBNLBOT.Exceptions.DisconnectedException;

public class packet {
	private InputStream in;
	private OutputStream out;
	public int pid;
	public Buffer data;

	private packet(int pid, byte[] data) {
		this.pid = pid;
		this.data = new Buffer(data);
	}

	public packet(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
	}

	protected packet() {
	}

	public void send(Buffer buff) throws IOException {
		byte[] b=addHeader(buff);
		out.write(ByteBuffer.allocate(b.length+buff.getData().length).put(b).put(buff.getData()).array());
		out.flush();
	}

	public packet getNextPacket() throws DisconnectedException, IOException {
		int inp = in.read();
		if (inp == -1) {
			throw new DisconnectedException();
		}
		int len = (in.read() << 8 | inp) - 3;
		byte pid = (byte) in.read();
		byte[] data = new byte[len];
		in.read(data);
		packet pack = new packet(pid, data);
		return pack;
	}

	public byte readByte() throws IOException {
		return (byte) in.read();
	}

	public short readShort() throws IOException {
		return (short) (readByte() | (readByte() << 8));
	}

	private byte[] addHeader(Buffer buff) {
		return new byte[] { ((byte) ((buff.getLength() + 3) & 0x00FF)), (byte) ((buff.getLength() + 3) >> 8), buff.getPid() };
	}

	public int readDword() throws IOException {
		return (in.read()) | (in.read() << 8) | (in.read() << 16) | (in.read() << 24);
	}
}
