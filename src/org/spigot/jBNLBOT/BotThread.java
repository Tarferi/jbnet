package org.spigot.jBNLBOT;

public class BotThread extends Thread {
	private String currentGame = "";
	private bot bot;

	public BotThread(bot bot) {
		this.bot = bot;
		this.setName("Bot Thread");
	}

	public void gamesReceived(StarcraftGame[] games) {
		if (games != null) {
			for (StarcraftGame game : games) {
				bot.requestPing(game);
				bot.udpsess.addGame(game);
				if (game.GameTime < 6 && !game.CdKeyChecksum.equals("Nope LOL")) {
					log("Game: " + game.GameName + " IP: " + game.Host.IP + ":" + game.Host.Port + " Host: " + game.Host.Name + " KeyHASH: " + game.CdKeyChecksum);
				}
			}
			if (games[0].GameName.equals(currentGame)) {
				if (games.length > 1) {
					// bot.joinGame(games[1].GameName, "", "iThief",
					// "Deletion");
					currentGame = games[1].GameName;
				}
			} else {
				// bot.joinGame(games[0].GameName, "", "iThief", "Deletion");
				currentGame = games[0].GameName;
			}
		}
		bot.udpsess.refreshGames();
	}

	@Override
	public void run() {
		Object o = new Object();
		synchronized (o) {
			try {
				o.wait(1000);
				bot.createGame("Thief's Lullaby", "", "iThief", "Lullaby");
				while (true) {
					o.wait(5000);
					//log("Requesting game list");
					bot.requestGameList();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void log(String message) {
		System.out.println("[BotThread] " + message);
	}
}
