package org.spigot.jBNLBOT.Exceptions;

public class AccountClosedException extends Exception {
	private static final long serialVersionUID = 1L;

	public AccountClosedException(String reason) {
		super(reason);
	}
}
