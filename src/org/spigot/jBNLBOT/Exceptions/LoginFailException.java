package org.spigot.jBNLBOT.Exceptions;

public class LoginFailException extends Exception {
	private static final long serialVersionUID = 1L;
	public LoginFailException(String reason) {
		super(reason);
	}
}
