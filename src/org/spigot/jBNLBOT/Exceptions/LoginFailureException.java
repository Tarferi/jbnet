package org.spigot.jBNLBOT.Exceptions;

public class LoginFailureException extends Exception{
	public LoginFailureException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
