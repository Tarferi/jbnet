package org.spigot.jBNLBOT;

public class GamePlayer {
	public final String IP;
	public final int Port;
	public final String Name;
	
	public GamePlayer(String IP, int Port, String Name) {
		this.IP=IP;
		this.Port=Port;
		this.Name=Name;
	}
}
