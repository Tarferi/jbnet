package org.spigot.jBNLBOT;

public enum GameType {
	Custom(1), Melee(2), FreeForAll(3), OneVsOne(4), CaptureTheFlag(5), Greed(6), Slaughter(7), SuddenDeath(8), Ladder(9), IronManLadder(0x10), UseMapSettings(0x11), TeamMelee(0x12), TeamFFA(0x13), TeamCTF(0x14), TopVsBottom(0x15), Unknown(0x20);

	public int type;

	GameType(int c) {
		type = c;
	}

	public static GameType fromID(String id) {
		switch (id) {
			case "1":
				return Custom;
			case "2":
				return Melee;
			case "3":
				return FreeForAll;
			case "4":
				return OneVsOne;
			case "5":
				return CaptureTheFlag;
			case "6":
				return Greed;
			case "7":
				return Slaughter;
			case "8":
				return SuddenDeath;
			case "9":
				return Ladder;
			case "a":
				return IronManLadder;
			case "b":
				return UseMapSettings;
			case "c":
				return TeamMelee;
			case "d":
				return TeamFFA;
			case "e":
				return TeamCTF;
			case "f":
				return TopVsBottom;
			default:
				return Unknown;
		}
	}
}
