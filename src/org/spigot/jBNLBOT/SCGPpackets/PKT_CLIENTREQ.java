package org.spigot.jBNLBOT.SCGPpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.GamePlayer;
import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class PKT_CLIENTREQ extends packet {

	public static void write(packet pack, GamePlayer player) throws IOException {
		Buffer buff = new Buffer((byte) 3);
		buff.target=player;
		buff.putDword(0);
		pack.sendRaw(buff);
	}
}
