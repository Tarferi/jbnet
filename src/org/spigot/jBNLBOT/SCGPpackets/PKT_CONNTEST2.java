package org.spigot.jBNLBOT.SCGPpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class PKT_CONNTEST2 extends packet {

	public static void write(packet pack, int serverToken, int UDPvalue) throws IOException {
		Buffer buff = new Buffer((byte) 0x09);
		buff.putDword(serverToken);
		buff.putDword(UDPvalue);
		pack.sendRaw(buff);
	}
}
