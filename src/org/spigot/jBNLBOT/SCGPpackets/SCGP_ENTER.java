package org.spigot.jBNLBOT.SCGPpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.GamePlayer;
import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class SCGP_ENTER extends packet {

	public static void write(packet pack, GamePlayer player, String username, String password) throws IOException {
		Buffer buff = new Buffer((byte) 0x07, (byte) 0);
		buff.target=player;
		buff.putNTString(username);
		buff.putNTString("PXES 0 0 0 0 0 0 0 0 PXES");
		buff.putNTString(password);
		pack.send(buff);
	}

}
