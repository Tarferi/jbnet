package org.spigot.jBNLBOT.SCGPpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class SCGP_PONG extends packet {

	public static void write(packet packet, packet pack) throws IOException {
		Buffer buff = new Buffer((byte) 0x05, (byte) 0x00);
		buff.target=pack.target;
		packet.send(buff);
	}
}
