package org.spigot.jBNLBOT.SCGPpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.GamePlayer;
import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class SCGP_REQUESTJOINOK extends packet {

	
	public static void write(packet pack,GamePlayer player) throws IOException {
		Buffer buff = new Buffer((byte) 0x02, (byte) 0);
		buff.target=player;
		buff.putDword(1);
		pack.send(buff);
	}
}
