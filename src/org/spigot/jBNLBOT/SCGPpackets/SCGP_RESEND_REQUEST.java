package org.spigot.jBNLBOT.SCGPpackets;

import java.io.IOException;

import org.spigot.jBNLBOT.BNETpackets.Buffer;

public class SCGP_RESEND_REQUEST extends packet {

	public static void write(packet packet) throws IOException {
		Buffer buff = new Buffer((byte) 0x00, (byte) 0);
		buff.status = 2;
		buff.increment = false;
		packet.send(buff);
	}
}
