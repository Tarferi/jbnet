package org.spigot.jBNLBOT.SCGPpackets;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;

import org.spigot.jBNLBOT.GamePlayer;
import org.spigot.jBNLBOT.StarcraftGame;
import org.spigot.jBNLBOT.UDPQueue;
import org.spigot.jBNLBOT.UDPSession;
import org.spigot.jBNLBOT.BNETpackets.Buffer;
import org.spigot.jBNLBOT.Exceptions.UDPMalformedPacketException;

public class packet {
	private DatagramSocket sock;
	public int pid;
	public int cid;
	public Buffer data;
	public byte[] rawdata;
	private SocketAddress address;
	public GamePlayer target;
	private UDPSession sess;
	private HashMap<String, UDPQueue> queues = new HashMap<>();
	public StarcraftGame game;

	private packet(int cid, int pid, byte[] data, GamePlayer socketAddress) {
		this.pid = pid;
		this.cid = cid;
		this.data = new Buffer(data);
		this.rawdata = data;
		target = socketAddress;
	}

	public packet(DatagramSocket out, SocketAddress address, UDPSession sess) {
		this.sock = out;
		this.address = address;
		this.sess = sess;
	}

	public packet() {
	}

	public synchronized void sendRaw(Buffer buff) throws IOException {
		byte[] b = new byte[] { buff.getPid(), 0, 0, 0 };
		byte[] a = buff.getData();
		byte[] finalbytes = ByteBuffer.allocate(b.length + a.length).put(b).put(a).array();
		DatagramPacket pack;
		if (buff.hostdiffers()) {
			pack = new DatagramPacket(finalbytes, finalbytes.length, InetAddress.getByName(buff.target.IP), buff.target.Port);
		} else {
			pack = new DatagramPacket(finalbytes, finalbytes.length, address);
		}
		sock.send(pack);
	}

	/**
	 * 
	 * @param IP IP To be parsed
	 * @param Port Port to be send
	 * @param in True=send, False=received
	 * @return Returns array, index 0 = sent, index 1 = received
	 */
	private UDPQueue getQueue(String IP, int Port) {
		IP = IP.replace("/", "");
		String hash = IP + ":" + Port;
		UDPQueue r;
		if (queues.containsKey(hash)) {
			r = queues.get(hash);
		} else {
			StarcraftGame game=sess.getGameBySocket(IP,Port);
			r = new UDPQueue(sock, game, this.sess);
			queues.put(hash, r);
		}
		return r;
	}

	private UDPQueue getQueue(DatagramPacket pack) {
		return getQueue(pack.getAddress().toString(), pack.getPort());
	}

	public void resetCounters(String IP, int Port) {
		IP = IP.replace("/", "");
		String hash = IP + ":" + Port;
		if (queues.containsKey(hash)) {
			queues.remove(hash);
		}
	}

	public synchronized void send(Buffer buff) throws IOException {
		byte[] b = addHeader(buff, getQueue(buff.target.IP, buff.target.Port));
		byte[] a = buff.getData();
		byte[] finalbytes = ByteBuffer.allocate(b.length + a.length).put(b).put(a).array();

		DatagramPacket pack;
		if (buff.hostdiffers()) {
			InetAddress inet = InetAddress.getByName(buff.target.IP);
			System.out.println("SENT: "+Buffer.getDump(finalbytes));
			pack = new DatagramPacket(finalbytes, finalbytes.length, inet, buff.target.Port);
		} else {
			pack = new DatagramPacket(finalbytes, finalbytes.length, address);
		}
		getQueue(pack).sentPacket(pack);
	}

	public packet getNextPacketRaw() throws IOException, UDPMalformedPacketException {
		byte[] meta = new byte[20486];
		DatagramPacket datagram = new DatagramPacket(meta, meta.length);
		sock.receive(datagram);
		ByteBuffer buff = ByteBuffer.wrap(datagram.getData());
		buff.order(ByteOrder.LITTLE_ENDIAN);
		int code = buff.getInt();
		if (code == 0) {
			buff.getShort(); // Checksum
			int length = (buff.getShort() & 0xffffffff);
			int recv = buff.getShort();
			int sent = buff.getShort();
			UDPQueue queue = getQueue(datagram);
			byte command = buff.get();
			byte packetID = buff.get();
			byte playerid = buff.get();
			byte status = buff.get();
			byte[] data;
			if (length >= 12) {
				data = new byte[length - 12];
				try {
					buff.get(data);
					if (command == 1 || command == 2) {
						if (status == 0) {
							packetID = data[0];
						}
					}
				} catch (BufferUnderflowException e) {
					System.err.println("Error happened");
				}
			} else {
				data = new byte[0];
			}
			StarcraftGame game = sess.getGameBySocket(datagram.getAddress(), datagram.getPort());
			GamePlayer player = null;
			if (game != null) {
				player = game.Host;
			}
			packet pack = new packet(command, packetID, data, player);
			pack.data.status = status;
			pack.data.cid = command;
			pack.data.sent = sent;
			pack.data.recv = recv;
			pack.data.playerID = playerid;
			pack.game=queue.game;
			pack.target=sess.getPlayer(datagram.getAddress(), datagram.getPort());
			System.out.println("RECV: "+Buffer.getDump(meta));
			return queue.parsePacket(pack, datagram);
		} else {
			StarcraftGame game = sess.getGameBySocket(datagram.getAddress(), datagram.getPort());
			GamePlayer player = null;
			if (game != null) {
				player = game.Host;
			}
			return new packet(4, code, datagram.getData(), player);
		}
	}

	public byte getStatus() {
		return data.status;
	}

	public byte[] addHeader(Buffer buff, UDPQueue queue) {
		byte[] total = new byte[16];
		byte[] data = buff.getData();
		total[0] = total[1] = total[2] = total[3] = 0; // 0x00
		int length = data.length + 12;
		total[6] = (byte) (length & 0xff);
		total[7] = (byte) ((length >> 8) & 0xff);
		total[8] = (byte) ((queue.getSent()) & 0xff); // Sent
		total[9] = (byte) (((queue.getSent()) >> 8) & 0xff);
		total[10] = (byte) ((queue.getReceived()) & 0xff); // Receivd
		total[11] = (byte) (((queue.getReceived()) >> 8) & 0xff);
		total[12] = (byte) buff.cid;
		total[13] = (byte) buff.pid;
		total[14] = (byte) queue.game.PlayerID;
		total[15] = buff.status;
		int A = 0;
		int B = 0;
		int C = 0;
		for (int i = data.length - 1; i >= 0; i--) {
			B = B + (data[i] & 0xff);
			if (B > 0xFF) {
				B = B - 0xFF;
			}
			A = A + B;
		}
		for (int i = total.length - 1; i >= 6; i--) {
			B = B + (total[i] & 0xff);
			if (B > 0xFF) {
				B = B - 0xFF;
			}
			A = A + B;
		}
		C = (B << 8) | (A % 255);
		A = 0xFF - ((C & 0xFF) + (C >> 8)) & 0xFF;
		B = (A + (C >> 8));
		B = 0xFF - (B & 0xFF);
		B = B | (A << 8);
		int CheckSum = (int) (B & 0xFFFF);
		total[4] = (byte) (CheckSum & 0xff);
		total[5] = (byte) ((CheckSum >> 8) & 0xff);
		return total;
	}
}