package org.spigot.jBNLBOT;

public class StarcraftGame {
	public final GameType GameType;
	public final short AddressFamily;
	// public final int Port;
	// public final String IP;
	public final int GameTime;
	public final GamePlayer Host;
	public final String GameName, GamePassword;
	public final boolean SavedGame;
	public final int MaxPlayers;
	public final int Speed;
	public final String CdKeyChecksum;
	public final String MapDimensions;
	// public final String HostName;
	public final String MapName;
	public final int TimeAdded;
	private final String stamp;
	public int PlayerID=0xff;

	public String getStamp() {
		return stamp;
	}
	
	public boolean Validate(int stamp) {
		return TimeAdded>stamp;
	}
	
	public StarcraftGame(int addrfam, int port, int ip, int time, String name, String pw, String stat) {
		TimeAdded = (int) (System.currentTimeMillis() / 1000);
		AddressFamily = (short) addrfam;
		int Port = ((port & 0xff00) >> 8) | ((port & 0xff) << 8);
		String IP = ((ip >> 0) & 0xff) + "." + ((ip >> 8) & 0xff) + "." + ((ip >> 16) & 0xff) + "." + ((ip >> 24) & 0xff);
		GameTime = time;
		GameName = name;
		GamePassword = pw;
		String[] ruth = stat.split(",");
		SavedGame = ruth[0] != "";
		int x, y;
		if (ruth[1].equals("")) {
			MapDimensions = "Unknown";
		} else {
			int a = Integer.parseInt(ruth[1]);
			x = a / 10;
			y = a % 10;
			MapDimensions = (x * 32) + "x" + (y * 32);
		}
		if (ruth[2].equals("")) {
			MaxPlayers = 8;
		} else {
			MaxPlayers = Integer.parseInt(ruth[2]) - 10;
		}
		Speed = Integer.parseInt(ruth[3]);
		GameType = org.spigot.jBNLBOT.GameType.fromID(ruth[5]);
		CdKeyChecksum = ruth[8];
		String[] r = ruth[11].split("" + Character.toString((char) 0x0d));
		String HostName = r[0];
		MapName = r[1];
		Host = new GamePlayer(IP, Port, HostName);
		stamp=IP+":"+Port;//+HostName;
	}
}
