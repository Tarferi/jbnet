package org.spigot.jBNLBOT;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.List;

import org.spigot.jBNLBOT.SCGPpackets.SCGP_RESEND_REQUEST;
import org.spigot.jBNLBOT.SCGPpackets.packet;

public class UDPQueue {
	private int sent = 0;
	private int recv = 1;
	private List<packet> packets = new ArrayList<>();
	private List<DatagramPacket> cache = new ArrayList<>();
	private DatagramSocket sock;
	public StarcraftGame game;
	private UDPSession sess;

	public UDPQueue(DatagramSocket sock, StarcraftGame game, UDPSession ses) {
		this.sock = sock;
		this.game = game;
		this.sess=ses;
	}

	public packet tryNext() throws IOException {
		for (packet pack : packets) {
			if (pack.data.recv == recv) {
				packets.remove(pack);
				recv = pack.data.recv;
				System.out.println("Removed packet number " + pack.data.recv);
				recv++;
				return pack;
			}
		}
		SCGP_RESEND_REQUEST.write(sess.packet);
		System.out.println("Skipping packet processing");
		return null;
	}

	public int getReceived() {
		return recv;
	}

	public int getSent() {
		return sent;
	}

	public void sentPacket(DatagramPacket packet) throws IOException {
		cache.add(packet);
		sent++;
		sendPacketQ(packet);
	}

	private void sendPacketQ(DatagramPacket packet) throws IOException {
		sock.send(packet);
		sock.send(packet);
		sock.send(packet);
		sock.send(packet);
		sock.send(packet);
	}

	private void resentPackets() throws IOException {
		for (DatagramPacket pack : cache) {
			sock.send(pack);
		}
	}

	public packet parsePacket(packet pack, DatagramPacket packet) throws IOException {
		if (pack.data.status == 2 && pack.data.cid == 0) {
			System.out.println("Requested packet resent");
			resentPackets();
			return null;
		} else if (pack.data.status == 1 && pack.data.cid == 0) {
			System.out.println("Verification requested");
			sendPacketQ(packet);
			return null;
		}
		if (pack.data.cid == 2) {
			return pack;
		}
		if (pack.data.cid == 1) {
			return pack;
		}
		if (pack.data.sent == sent) {
			cache.clear();
		}
		if (recv > pack.data.recv) {
			// System.out.println("Received duplicate packet " + recv);
			return null;
		}
		if (recv == pack.data.recv) {
			recv++;
			return pack;
		} else {
			System.out.println("Added packet number " + pack.data.recv);
			packets.add(pack);
			return tryNext();
		}
	}

}
