package org.spigot.jBNLBOT;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;

import org.spigot.jBNLBOT.BNETpackets.Buffer;
import org.spigot.jBNLBOT.Exceptions.UDPMalformedPacketException;
import org.spigot.jBNLBOT.SCGPpackets.PKT_CLIENTREQ;
import org.spigot.jBNLBOT.SCGPpackets.PKT_CONNTEST2;
import org.spigot.jBNLBOT.SCGPpackets.SCGP_ENTER;
import org.spigot.jBNLBOT.SCGPpackets.SCGP_GAMEDATA;
import org.spigot.jBNLBOT.SCGPpackets.SCGP_PONG;
import org.spigot.jBNLBOT.SCGPpackets.SCGP_REQUESTJOIN;
import org.spigot.jBNLBOT.SCGPpackets.SCGP_REQUESTJOIN2;
import org.spigot.jBNLBOT.SCGPpackets.packet;

public class UDPSession extends Thread {
	@SuppressWarnings("unused")
	private BotThread thread;
	private bot bot;
	private int port;
	public DatagramSocket server;
	public packet packet;
	private boolean tenb = false;
	private HashMap<String, StarcraftGame> games = new HashMap<>();
	private HashMap<String, GamePlayer> players = new HashMap<>();
	private boolean joined = false;

	public UDPSession(bot bot, BotThread thread, int port) {
		this.bot = bot;
		this.thread = thread;
		this.port = port;
		this.setName("UDP Thread");
		start();
	}

	public GamePlayer getPlayer(InetAddress addr, int port) {
		String hash = addr.toString().replace("/", "") + ":" + port;
		if (players.containsKey(hash)) {
			return players.get(hash);
		} else {
			GamePlayer player = new GamePlayer(addr.toString().replace("/", ""), port, null);
			players.put(hash, player);
			return player;
		}
	}

	@Override
	public void run() {
		try {
			serverStart();
		} catch (IOException | UDPMalformedPacketException e) {
			e.printStackTrace();
		}
	}

	protected void addGame(StarcraftGame game) {
		synchronized (games) {
			if (!games.containsKey(game.getStamp())) {
				games.put(game.getStamp(), game);
			}
		}
	}

	protected void refreshGames() {
		int timetodie = 300;
		int stamp = (int) (System.currentTimeMillis() / 1000);
		stamp -= timetodie;
		HashMap<String, StarcraftGame> gamess = new HashMap<>();
		for (StarcraftGame game : games.values()) {
			if (game.Validate(stamp)) {
				gamess.put(game.getStamp(), game);
			}
		}
		synchronized (games) {
			games = gamess;
		}
	}

	public StarcraftGame getGameBySocket(InetAddress sock, int port) {
		return getGameBySocket(sock.getHostAddress(), port);
	}

	public StarcraftGame getGameBySocket(String IP, int port) {
		String stamp = IP.replace("/", "") + ":" + port;
		if (games.containsKey(stamp)) {
			return games.get(stamp);
		}
		return null;
	}

	public void sendPingRequest(GamePlayer player) throws IOException {
		PKT_CLIENTREQ.write(packet, player);
	}

	public void serverStart() throws IOException, UDPMalformedPacketException {
		server = new DatagramSocket(port);
		packet = new packet(server, bot.getServerAddress(), this);
		while (true) {
			packet pack = packet.getNextPacketRaw();
			if (pack == null) {
				continue;
			}
			if (pack.cid == 4) {
				byte[] meta = pack.rawdata;
				switch (pack.pid) {
					default:
						log("Received unknown UDP 0 packet (" + pack.pid + ")");
						log(Buffer.getDump(meta));
					break;
					case 3:
						// int ping = (meta[0] & 0xff) | ((meta[1] << 8) & 0xff) | ((meta[2] << 16) & 0xff) | ((meta[3] << 24) & 0xff);
						// log("Received PING: " + ping);
						if (!joined) {
							log("Requesting join");
							joined = true;
							SCGP_REQUESTJOIN.write(packet, pack.target);
						}
					case 5:
						int UDPtenb = (meta[0] & 0xff) | ((meta[1] << 8) & 0xff) | ((meta[2] << 16) & 0xff) | ((meta[3] << 24) & 0xff);
						if (!tenb) {
							log("Received Tenb (" + UDPtenb + ")");
							tenb = true;
						}
					break;
				}
			} else if (pack.cid == 0) {
				// SCGP Protocol
				switch (pack.pid) {
					case 0:
						// Update counts
					case 1:
					// Somebody requested to join out game
					break;
					case 2:
						// Out join request has been accepted
						log("Joining game...");
						SCGP_REQUESTJOIN2.write(packet, pack.target);
						SCGP_ENTER.write(packet, pack.target, "Copy of iThief", "");
					break;
					case 4: // Ping
						SCGP_PONG.write(packet, pack);
					break;
					case 6:
						log("Received player");
					break;
					case 8:
						SCGP_GAMEDATA.read(pack.data, pack.game);
						log("Received player ID: " + pack.game.PlayerID);
					break;
					case 9:
						log("Received game data");
					break;
					default:
						log("Received unknown SCGP packet ID: " + pack.pid);
					break;

				}
			} else if (pack.cid == 1) {
				switch (pack.pid) {
					case 74:
					case 75:
					case 76:
						Buffer.getDump(pack.rawdata);
					// chat
					break;
					default:
						log("Received unknown SCGP1 packet ID: " + pack.pid);
					break;
				}
			} else if (pack.cid == 2) {
				switch (pack.pid) {
					case 0:
					// NOP
					break;
					case 5:
					// NOP
					break;
					default:
						log("Received unknown SCGP2 ID: " + pack.pid);
					break;
				}
			} else {
				log("Received packet command " + pack.cid + " ID: " + pack.pid);
			}
		}
	}

	public void sendUDPTenb(int ServerToken, int UDPValue) {
		try {
			PKT_CONNTEST2.write(packet, ServerToken, UDPValue);
			PKT_CONNTEST2.write(packet, ServerToken, UDPValue);
			PKT_CONNTEST2.write(packet, ServerToken, UDPValue);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void log(String message) {
		System.out.println("[UDP] " + message);
	}
}
