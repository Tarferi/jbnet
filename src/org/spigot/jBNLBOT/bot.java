package org.spigot.jBNLBOT;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;

import org.spigot.jBNLBOT.BNETpackets.SID_AUTH_CHECK;
import org.spigot.jBNLBOT.BNETpackets.SID_AUTH_INFO;
import org.spigot.jBNLBOT.BNETpackets.SID_GETADVLISTEX;
import org.spigot.jBNLBOT.BNETpackets.SID_LOGONRESPONSE2;
import org.spigot.jBNLBOT.BNETpackets.SID_STARTADVEX3;
import org.spigot.jBNLBOT.BNETpackets.SID_AUTH_INFO.AUTH_INFO;
import org.spigot.jBNLBOT.BNETpackets.SID_PING;
import org.spigot.jBNLBOT.BNETpackets.packet;
import org.spigot.jBNLBOT.BNLSpackets.BNLS_VERSIONCHECKEX2.BNLS_AUTH;
import org.spigot.jBNLBOT.Exceptions.AccountClosedException;
import org.spigot.jBNLBOT.Exceptions.DisconnectedException;
import org.spigot.jBNLBOT.Exceptions.InvalidPasswordException;
import org.spigot.jBNLBOT.Exceptions.LoginFailException;
import org.spigot.jBNLBOT.Exceptions.LoginFailureException;
import org.spigot.jBNLBOT.Exceptions.UserDoesNotExistException;

public class bot extends Thread {
	@SuppressWarnings("unused")
	private String username, password, key, gameid, server;
	private int ServerToken;
	private String bnls = "phix.no-ip.org";
	private packet packet;
	private BotThread thr = new BotThread(this);
	public byte[] localip;
	public UDPSession udpsess;
	private Socket s;

	public bot(String username, String password, String cdkey, String server, String gameid) {
		this.setName("Main");
		this.username = username;
		this.password = password;
		this.key = cdkey;
		this.gameid = gameid;
		this.server = server;
		start();
	}

	public void requestPing(StarcraftGame game) {
		try {
			udpsess.sendPingRequest(game.Host);
		} catch (IOException e) {
			log("Address for game ping could not be resolved");
		}
	}

	@Override
	public void run() {
		try {
			connect();
		} catch (SocketException e) {
			log("Server has closed connection. Probably banned");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SocketAddress getServerAddress() {
		return s.getRemoteSocketAddress();
	}

	public void requestGameList() throws IOException {
		SID_GETADVLISTEX.write(packet);
	}

	public void createGame(String gameName, String password, String hostName, String mapName) throws IOException {
		log("Creating game: " + gameName);
		SID_STARTADVEX3.write(packet, gameName, password, mapName, hostName, 2, 2);
	}

	public void joinGame(StarcraftGame game) {

	}

	public void connect() throws SocketException {
		s = null;
		InputStream in = null;
		OutputStream out = null;
		try {
			s = new Socket(server, 6112);
			localip = s.getLocalAddress().getAddress();
			in = s.getInputStream();
			out = s.getOutputStream();
			packet = new packet(in, out);
			udpsess = new UDPSession(this, thr, 61112);
			out.write((byte) 1);
			SID_AUTH_INFO.Write(packet, localip);
			BNLS_AUTH auth = null;
			boolean cont = true;
			while (cont) {
				packet pack = packet.getNextPacket();
				if (pack == null) {
					log("Ignoring empty packet");
					continue;
				}
				switch (pack.pid) {
					default:
						log("Unknown code: " + pack.pid);
					break;
					case packetlist.SID_PING:
						SID_PING.write(packet, pack.data.getDword());
					break;
					case packetlist.SID_AUTH_INFO:
						AUTH_INFO inf = SID_AUTH_INFO.Read(pack.data, key);
						ServerToken = inf.ServerToken;
						udpsess.sendUDPTenb(inf.ServerToken, inf.UDPValue);
						log("BNLS hashing in progress...");
						auth = BNLSHandler.getData(inf, bnls);
						SID_AUTH_CHECK.Write(packet, auth, ServerToken, key);
					break;
					case packetlist.SID_AUTH_CHECK:
						try {
							SID_AUTH_CHECK.Read(pack.data);
							log("Connected to Battle.net");
							SID_LOGONRESPONSE2.write(packet, ServerToken, auth.CdKeyHash.ClientToken, password, username);
						} catch (LoginFailureException e) {
							String msg = e.getMessage();
							log("Failed to connect because: " + msg);
							cont = false;
						}
					break;
					case packetlist.SID_LOGONRESPONSE2:
						try {
							SID_LOGONRESPONSE2.read(pack.data);
							log("Logged in");
							thr.start();
						} catch (AccountClosedException e) {
							log("Account closed: " + e.getMessage());
						} catch (InvalidPasswordException e) {
							log("Invalid password");
						} catch (UserDoesNotExistException e) {
							log("Account does not exist");
						} catch (LoginFailException e) {
							log("Login failed: " + e.getMessage());
						}
					break;
					case packetlist.SID_SETEMAIL:
						log("Email set is requested");
					break;
					case packetlist.SID_GETADVLISTEX:
						thr.gamesReceived(SID_GETADVLISTEX.read(pack.data));
					break;
					case packetlist.SID_STARTADVEX3:
						if (SID_STARTADVEX3.read(pack.data)) {
							log("Entered game");
						} else {
							log("Game entering failed");
						}
					break;
				}
			}
		} catch (SocketException e) {
			throw e;
		} catch (DisconnectedException e) {
			log("Disconnected");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			in.close();
		} catch (Exception e) {
		}
		try {
			out.close();
		} catch (Exception e) {
		}
		try {
			s.close();
		} catch (Exception e) {
		}
	}

	private static final void log(String message) {
		MAIN.log(message);
	}
}
